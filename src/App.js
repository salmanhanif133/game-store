import React, { useState } from "react";
import "./styles/app.css";
import Main from "./components/Main";
import { GET_URL } from "./helpers";
const axios = require("axios");

export default function App() {
  const [loading, setLoading] = useState(true);
  const [userData, setUserData] = React.useState(false);
  React.useEffect(() => {
    const Authentication = async () => {
      let userToken = JSON.parse(localStorage.getItem("accessToken"));
      let userEmail = JSON.parse(localStorage.getItem("email"));

      userToken = JSON.parse(localStorage.getItem("accessToken"));
      try {
        const response = await axios.get(`${GET_URL()}/api/getUser`, {
          headers: { Authorization: "Bearer " + userToken },
        });
        // if (response.data.Message) {
        //   console.log(response.data.Message)
        //   return false
        // }
        console.log(response, " get user response");
        setUserData(response.data);
      } catch (error) {
        console.error(error);
        setUserData(null);
      }
    };

    Authentication();
  }, []);
  return (
    <>
      <Main userData={userData} loading={loading} />
    </>
  );
}
