import React, { useEffect, useState } from "react";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from "react-loader-spinner";
import { Link, useParams } from "react-router-dom";
import NavBar from "./NavBar";
import Authentication from "./Authentication";
import { GET_URL } from "../helpers";
const axios = require("axios");

const BreadCrumbs = ({ adProps, loading }) => {
  let secondLevel = "";
  let linkk = "#";
  if (adProps.category) {
    if (adProps && adProps.category.split(" ")[0] === "Xbox") {
      secondLevel = "Xbox";
      linkk = `/?name=all&category=Xbox&location=all`;
    } else if (adProps && adProps.category.split(" ")[0] === "Play") {
      secondLevel = "Play station";
      linkk = `/?name=all&category=Play station&location=all`;
    }
  }
  return (
    <div className="bg-white flex items-center justify-center border-b border-t">
      <nav className="text-black font-bold py-4" aria-label="Breadcrumb">
        {loading ? (
          "Loading"
        ) : (
            <ol className="list-none p-0 inline-flex">
              <li className="flex items-center">
                <Link to="/">Home</Link>
                <svg
                  className="fill-current w-3 h-3 mx-3"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 320 512"
                >
                  <path d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" />
                </svg>
              </li>
              <li className="flex items-center">
                <a href={`${linkk}`}>{secondLevel} </a>
                <svg
                  className="fill-current w-3 h-3 mx-3"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 320 512"
                >
                  <path d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" />
                </svg>
              </li>
              <li>
                <a href="#" className="text-gray-500" aria-current="page">
                  {adProps.category}
                </a>
              </li>
            </ol>
          )}
      </nav>
    </div>
  );
};

export default function Post({ userData }) {
  const [adProps, setAdProps] = useState([]);
  const [loading, setLoading] = useState(true);
  const [isEdit, setEdit] = useState(false);
  const [userResult, setuserResult] = React.useState(null);
  let { id } = useParams();

  useEffect(() => {
    async function getAd() {
      try {
        const response = await axios.get(`${GET_URL()}/api/post/${id}`);
        setAdProps(response.data.result[0]);
        setuserResult(response.data.userResult);
        setLoading(false);
        console.log(response.data);
      } catch (error) {
        console.error(error);
      }
    }
    getAd();
    // userData && adProps && (userData.userId && adProps.createdBy === userData.userId) && setEdit(true)
    console.log(userData);
    userData && userData.userAds.includes(id) && setEdit(true);
  }, [userData, isEdit]);
  return (
    <>
      {/* <Authentication authenticateUser={authenticateUser} /> */}
      <NavBar userData={userData} />
      <BreadCrumbs loading={loading} adProps={adProps} />
      {loading ? (
        <div className="flex justify-center mt-8 w-full items-center text-center mx-auto">
          <Loader
            type="TailSpin"
            color="#4299e1"
            height={50}
            width={50}
            timeout={90000} //3 secs
          />
        </div>
      ) : (
          <>
            <AdDetails
              adProps={adProps}
              userData={userData}
              isEdit={isEdit}
              userResult={userResult}
              adImage={adProps.adImage}
            />
            {userData && !isEdit ? <SendMessage userData={userData} /> : <></>}
          </>
        )}
    </>
  );
}

const SendMessage = ({ userData }) => {
  const [name, setname] = useState(
    `${userData.firstname} ${userData.lastname}`
  );
  const [email, setemail] = useState(`${userData.email}`);
  const [message, setmessage] = useState(
    "I saw your ad! It looks very nice! Let's discuss further"
  );
  const [phone, setphone] = useState("");

  return (
    <div className="px-10">
      <div className="flex">
        <div className="w-3/4 mr-10 mb-8">
          <div className="border">
            <form className="p-10" method="post">
              <p className="text-center mb-4 text-2xl font-bold">
                Send your message
              </p>
              {/* <p className="text-center mb-4 text-base font-bold text-red-500">{checkError.Error}</p> */}
              {/* <p className="text-center mb-4 text-base font-bold text-green-500">{success}</p> */}
              <div className="md:items-center">
                <label className="block text-gray-800 font-bold  mb-1 md:mb-1 pr-4">
                  Your Message
                </label>
                <input
                  name="Name"
                  className="bg-white appearance-none border mb-4 border-gray-700 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-blue-500"
                  type="text"
                  value={message}
                  onChange={(e) => setmessage(e.target.value)}
                />
              </div>
              {/* <p className="text-center my-2 text-sm text-red-500 font-bold">{checkError.name}</p> */}
              <div className="md:items-center">
                <label className="block text-gray-800 font-bold  mb-1 md:mb-1 pr-4">
                  Your Name
                </label>
                <input
                  name="Name"
                  className="bg-white appearance-none border mb-4 border-gray-700 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-blue-500"
                  placeholder="Name"
                  type="text"
                  value={name}
                  onChange={(e) => setname(e.target.value)}
                />
              </div>
              <div className="md:items-center">
                <label className="block text-gray-800 font-bold  mb-1 md:mb-1 pr-4">
                  Your Email
                </label>
                <input
                  name="Name"
                  className="bg-white appearance-none border mb-4 border-gray-700 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-blue-500"
                  placeholder="Name"
                  type="text"
                  value={email}
                  onChange={(e) => setemail(e.target.value)}
                />
              </div>
              <div className="md:items-center">
                <label className="block text-gray-800 font-bold  mb-1 md:mb-1 pr-4">
                  Your Phone
                </label>
                <input
                  name="Name"
                  className="bg-white appearance-none border mb-4 border-gray-700 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-blue-500"
                  placeholder="Name"
                  type="text"
                  value={phone}
                  onChange={(e) => setphone(e.target.value)}
                />
              </div>
              {/* <p className="text-center my-2 text-sm text-red-500 font-bold">{checkError.price}</p> */}
              <div className="md:flex md:items-center justify-center mt-4">
                {/* <Link href="/"> */}
                <button
                  type="submit"
                  className="shadow bg-green-500 hover:bg-green-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded"
                >
                  Send Message
                </button>
                {/* </Link> */}
              </div>
            </form>
          </div>
        </div>
        <div className="w-1/4"></div>
      </div>
    </div>
  );
};

const AdDetails = ({ adProps, userData, isEdit, userResult, adImage }) => {
  const [name, setname] = useState(adProps.name);
  const [price, setprice] = useState(adProps.price);
  const [descriptionShort, setdescriptionShort] = useState(
    adProps.description.short
  );
  const [descriptionLong, setdescriptionLong] = useState(
    adProps.description.long
  );
  const [category, setCategory] = useState("Xbox");
  const [condition, setcondition] = useState("Used");
  const [checkError, setErrors] = useState("");
  const [success, setSuccess] = useState("");

  let userToken = JSON.parse(localStorage.getItem("accessToken"));
  const options = {
    headers: { Authorization: "Bearer " + userToken },
  };
  function applyChanges(id) {
    // setCheckErrors(true);
    axios
      .post(
        `${GET_URL()}/api/post/updateAd`,
        {
          name,
          price,
          descriptionShort,
          descriptionLong,
          category,
          condition,
          userId: id,
          adId: adProps._id,
        },
        options
      )
      .then(function (response) {
        console.log(response.data);
        setSuccess(response.data.Message);
        setErrors("");
        // window.location.href = "/";
      })
      .catch(function (error) {
        console.log(error.response, "Asd");
        setErrors(error.response.data);
        console.log(userData.userId);
      });
  }

  console.log(userData, "userdata");
  let featureBox;
  if (adProps && adProps.isFeatured) {
    featureBox = (
      <div className="w-1/3 bg-yellow-400 h-5 absolute text-xs uppercase font-bold text-center top-0 mt-4">
        Featured
      </div>
    );
  }

  if (isEdit) {
    return (
      <div className="px-10 py-10">
        <div className="text-center mb-4 ">
          <button
            className="bg-white border border-gray-400 hover:bg-blue-600 hover:text-white text-blue-600 font-semibold tracking-widest text-sm py-1 px-6 rounded mr-10"
            onClick={() => applyChanges(userData.userId)}
          >
            Apply
          </button>
        </div>
        <p className="text-center font-bold text-green-400">{success}</p>
        <div className="flex">
          <div className="w-3/4 mr-10">
            <div className="flex mb-4 relative border">
              {featureBox}
              <img
                className="mx-auto w-2/3 p-4"
                src={adProps.adImage}
                alt="Sunset in the mountains"
              />
            </div>
            <div className="border p-4">
              <p className="text-xl font-bold mb-2 text-gray-800">Details</p>
              <div className="flex justify-between text-base pb-4 mb-4 border-b text-gray-600">
                <p>Category: {adProps.category}</p>
                <input
                  type="text"
                  className="appearance-none border-b-2 border-blue-400 pb-2"
                  value={`Conditon: ${condition}`}
                  onChange={(e) => setcondition(e.target.value)}
                />
              </div>
              <p className="text-xl font-bold text-gray-800 mb-2">
                Description Long
              </p>
              <input
                type="text"
                className="appearance-none border-b-2 border-blue-400 pb-2 w-full"
                value={descriptionLong}
                onChange={(e) => setdescriptionLong(e.target.value)}
              />
              <p className="text-xl font-bold text-gray-800 mb-2">
                Description Short
              </p>
              <input
                type="text"
                className="appearance-none border-b-2 border-blue-400 pb-2 w-full"
                value={descriptionShort}
                onChange={(e) => setdescriptionShort(e.target.value)}
              />
            </div>
          </div>
          <div className="w-1/3">
            <div className="border p-4 mb-4">
              <p className="flex text-4xl font-bold">
                Rs:{" "}
                <input
                  type="text"
                  className="appearance-none border-b-2 border-blue-400 pb-2 text-4xl font-bold mb-4 w-full"
                  value={price}
                  onChange={(e) => setprice(e.target.value)}
                />
              </p>
              <input
                type="text"
                className="appearance-none border-b-2 border-blue-400 pb-2 text-lg text-gray-600 mb-8"
                value={name}
                onChange={(e) => setname(e.target.value)}
              />
              <div className="flex justify-between text-sm text-gray-600 mt-4">
                <p>
                  {adProps.location.country} | {adProps.location.city},{" "}
                  {adProps.location.province}, {adProps.location.area}
                </p>
                <p>{adProps.time}</p>
              </div>
            </div>
            <div className="border p-4 mb-4">
              <p className="text-3xl leading-7 mb-6">Description</p>
              {/* <p className="text-3xl leading-7 mb-6">Seller Description</p> */}
              <Link to={`/profile/${userResult.id}`}><div className="flex items-center mb-4">
                {userResult && <img className="h-16 w-16 mr-4 rounded-full" src={userResult.imagePath} />}
                <div className="text-left">
                  <p className="text-lg font-bold">{userResult.username}</p>
                  <p className="text-sm text-gray-600">Member since yesteday</p>
                </div>
              </div>
              </Link>
              <button className="py-2 w-full bg-blue-600 hover:bg-blue-700 rounded-full text-white font-semibold mb-4">
                Send Message
              </button>
              <div className="flex justify-center">
                <p className="text-center mr-4 font-bold">
                  {userResult.phonenumber}
                </p>
              </div>
            </div>
            <div className="border p-4 mb-4">
              <p className="text-2xl leading-7 font-bold mb-4">Map</p>
              <iframe
                width="350"
                height="250"
                frameBorder="0"
                src={`https://www.google.com/maps/embed/v1/search?q=${adProps.location.country}%20${adProps.location.city}%20${adProps.location.province}%20${adProps.location.area}&key=AIzaSyBnYLxcdJDGPzzyKCu2eRIra_ljDJ84UOQ`}
              ></iframe>
              <div className="flex justify-between text-sm text-gray-600 mt-4">
                <p>
                  {adProps.location.country} | {adProps.location.city},{" "}
                  {adProps.location.province}, {adProps.location.area}
                </p>
                <p>{adProps.time}</p>
              </div>
            </div>
            <div className="flex justify-center text-lg font-bold">
              <p>AD ID {adProps._id}</p>
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <div className="px-10 py-10">
        <div className="flex">
          <div className="w-3/4 mr-10">
            <div className="flex mb-4 relative border">
              {featureBox}
              <img
                className="mx-auto w-2/3 p-4"
                src={adProps.adImage}
                alt="Sunset in the mountains"
              />
            </div>
            <div className="border p-4">
              <p className="text-xl font-bold mb-2 text-gray-800">Details</p>
              <div className="flex justify-between text-base pb-4 mb-4 border-b text-gray-600">
                <p>Category: {adProps.category}</p>
                <p>Conditon: {adProps.condition}</p>
              </div>
              <p className="text-xl font-bold text-gray-800 mb-2">
                Description
              </p>
              <p className="text-base text-gray-700">
                {adProps.description.long}
              </p>
            </div>
          </div>
          <div className="w-1/3">
            <div className="border p-4 mb-4">
              <p className="text-4xl leading-7 font-bold mb-4">
                Rs {adProps.price}
              </p>
              <p className="text-lg text-gray-600 mb-8">{adProps.name}</p>
              <div className="flex justify-between text-sm text-gray-600">
                <p>
                  {adProps.location.country} | {adProps.location.city},{" "}
                  {adProps.location.province}, {adProps.location.area}
                </p>
                <p>{adProps.time}</p>
              </div>
            </div>
            <div className="border p-4 mb-4">
              <p className="text-3xl leading-7 mb-6">Seller Description</p>
              {/* <p className="text-3xl leading-7 mb-6">Seller Description</p> */}
              <Link to={`/profile/${userResult.id}`}><div className="flex items-center mb-4">
                {userResult.imagePath && <img className="h-16 w-16 mr-4 rounded-full" src={userResult.imagePath} />}
                <div className="text-left">
                  <p className="text-lg font-bold">{userResult.username}</p>
                  <p className="text-sm text-gray-600">Member since yesteday</p>
                </div>
              </div>
              </Link>
              {userData ? (
                <button className="py-2 w-full bg-blue-600 hover:bg-blue-700 rounded-full text-white font-semibold mb-4">
                  Send Message
                </button>
              ) : (
                  <Link to="/signin">
                    <button className="py-2 w-full bg-blue-300 hover:cursor-not-allowed rounded-full text-white font-semibold mb-4">
                      Send Message
                  </button>
                  </Link>
                )}
              {userData ? (
                <div className="flex justify-center">
                  {userResult.phonenumber && (
                    <p className="text-center mr-4 font-bold">
                      {userResult.phonenumber}
                    </p>
                  )}
                </div>
              ) : (
                  <div className="flex justify-center">
                    <p className="text-center mr-4 font-bold">*** **** ****</p>
                    <Link
                      to="/signin"
                      className="underline text-blue-600 hover:no-underline"
                    >
                      Show number
                  </Link>
                  </div>
                )}
            </div>
            <div className="border p-4 mb-4">
              <p className="text-2xl leading-7 font-bold mb-4">Map</p>
              <iframe
                width="350"
                height="250"
                frameBorder="0"
                src={`https://www.google.com/maps/embed/v1/search?q=${adProps.location.country}%20${adProps.location.city}%20${adProps.location.province}%20${adProps.location.area}&key=AIzaSyBnYLxcdJDGPzzyKCu2eRIra_ljDJ84UOQ`}
              ></iframe>
              <div className="flex justify-between text-sm text-gray-600 mt-4">
                <p>
                  {adProps.location.country} | {adProps.location.city},{" "}
                  {adProps.location.province}, {adProps.location.area}
                </p>
                <p>{adProps.time}</p>
              </div>
            </div>
            <div className="flex justify-center text-lg font-bold">
              <p>AD ID {adProps._id}</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
};
