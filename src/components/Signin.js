import React, { useState } from "react";
import { GET_URL } from "../helpers";
import { Link } from "react-router-dom";

const axios = require("axios");
export default function Signin() {
  function addUser(e) {
    e.preventDefault();
    // setCheckErrors(true);
    axios
      .post(`${GET_URL()}/api/signup`, {
        firstname,
        lastname,
        email,
        password,
        confirmPassword,
        username,
        phonenumber,
      })
      .then(function (response) {
        console.log(response.data);
        setSuccess(response.data.Message);
        setErrors("");
        window.location.replace("http://localhost:3000/#/login");
      })
      .catch(function (error) {
        console.log(error.response);
        setErrors(error.response.data);
      });
  }

  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setconfirmPassword] = useState("");
  const [username, setUsername] = useState("");
  const [phonenumber, setphonenumber] = useState("");
  const [checkError, setErrors] = useState("");
  const [success, setSuccess] = useState("");
  return (
    <div className="mx-auto max-w-lg mt-10">
      <form className="h-screen" onSubmit={addUser} method="post">
        <p className="text-center mb-4 text-2xl font-bold">Please Sign in</p>
        <p className="text-center mb-4 text-base font-bold text-red-500">
          {checkError.Error}
        </p>
        <p className="text-center mb-4 text-base font-bold text-green-500">
          {success}
        </p>
        <div className="md:flex md:items-center">
          <div className="md:w-1/4">
            <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
              First Name
            </label>
          </div>
          <div className="md:w-2/3">
            <input
              className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
              placeholder="First Name"
              type="text"
              value={firstname}
              onChange={(e) => setFirstname(e.target.value)}
            />
          </div>
        </div>
        <p className="text-center my-2 text-sm text-red-500 font-bold">
          {checkError.firstname}
        </p>
        <div className="md:flex md:items-center">
          <div className="md:w-1/4">
            <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
              Last Name
            </label>
          </div>
          <div className="md:w-2/3">
            <input
              className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
              type="text"
              placeholder="Last Name"
              value={lastname}
              onChange={(e) => setLastname(e.target.value)}
            />
          </div>
        </div>
        <p className="text-center my-2 text-sm text-red-500 font-bold">
          {checkError.lastname}
        </p>
        <div className="md:flex md:items-center">
          <div className="md:w-1/4">
            <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
              Email
            </label>
          </div>
          <div className="md:w-2/3">
            <input
              className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
              placeholder="Email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
        </div>
        <p className="text-center my-2 text-sm text-red-500 font-bold">
          {checkError.email}
        </p>
        <div className="md:flex md:items-center">
          <div className="md:w-1/4">
            <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
              Password
            </label>
          </div>
          <div className="md:w-2/3">
            <input
              className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
              type="password"
              placeholder="**********"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
        </div>
        <p className="text-center my-2 text-sm text-red-500 font-bold">
          {checkError.password}
        </p>
        <div className="md:flex md:items-center">
          <div className="md:w-1/4">
            <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
              Confirm Password
            </label>
          </div>
          <div className="md:w-2/3">
            <input
              className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
              type="password"
              placeholder="**********"
              value={confirmPassword}
              onChange={(e) => setconfirmPassword(e.target.value)}
            />
          </div>
        </div>
        <p className="text-center my-2 text-sm text-red-500 font-bold">
          {checkError.confirmPassword}
        </p>
        <div className="md:flex md:items-center">
          <div className="md:w-1/4">
            <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
              User name
            </label>
          </div>
          <div className="md:w-2/3">
            <input
              className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
              type="text"
              placeholder="User name"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
        </div>
        <p className="text-center my-2 text-sm text-red-500 font-bold">
          {checkError.username}
        </p>
        <div className="md:flex md:items-center">
          <div className="md:w-1/4">
            <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
              Phone number
            </label>
          </div>
          <div className="md:w-2/3">
            <input
              className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
              type="text"
              placeholder="User name"
              value={phonenumber}
              onChange={(e) => setphonenumber(e.target.value)}
            />
          </div>
        </div>
        <p className="text-center my-2 text-sm text-red-500 font-bold">
          {checkError.phonenumber}
        </p>
        <div className="md:flex md:items-center justify-center">
          {/* <Link href="/"> */}
          <button
            type="submit"
            className="shadow bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded"
          >
            Sign Up
          </button>
          {/* </Link> */}
        </div>
        <div className="text-center mt-8 font-bold text-blue-600 hover:underline">
          <Link to="/login">Already a member? login</Link>
        </div>
      </form>
    </div>
  );
}
