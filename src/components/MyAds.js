import React, { useEffect, useState } from 'react'
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import Loader from 'react-loader-spinner'
import {
  Link,
  useParams
} from "react-router-dom";
import NavBar from './NavBar'
// import adLogo from '../fifa20.jpg'
import Authentication from './Authentication';
import { GET_URL } from '../helpers';
const axios = require('axios');


const Table = ({ ad, deleteAd, applyChanges }) => {
  console.log(ad)
  return (

    <tr>
      <td className="border px-4 py-2 w-64 text-center font-bold text-blue-500 "> <Link to={`/ads/${ad._id}`} className="no-underline hover:underline">{ad.name}</Link></td>
      <td className="border border-r-0 px-4 py-2 w-64 text-center">{ad._id}</td>
      {ad.isRejected === 'true' ? <td className="border border-red-500 border-r-0 bg-red-200 text-red-500 px-4 py-2 w-64 text-center">Rejected</td> : ad.isApproved === 'true' ? <td className="border border-green-500 border-r-0 bg-green-200 text-green-500 px-4 py-2 w-64 text-center">Approved</td> : <td className="border border-orange-600 border-r-0 bg-orange-200 px-4 text-orange-500 py-2 w-64 text-center">Pending</td>}
      {ad.isRejected === 'true' && <td className="border border-r-0 border-gray-600 px-4 py-2 text-center"><button onClick={() => applyChanges(ad._id)}>Re-Submit</button></td>}
      <td className="border border-red-500 px-4 py-2 text-red-500 font-bold"><button onClick={() => deleteAd(ad._id)}>Delete</button></td>
    </tr>
  )
}


export default function MyAds({ userData }) {
  const [adProps, setAdProps] = useState([]);
  const [loading, setLoading] = useState(true);
  // const [idArray, setIdArray] = useState([]);
  let idArray = [];
  console.log(userData.userId)
  function getUserAd(e) {
    // setCheckErrors(true);
    axios.post(`${GET_URL()}/api/getUserAd`, {
      userId: userData.userId,
    }
    ).then(function (response) {
      console.log(response.data);
      setAdProps(response.data.result)
      setLoading(false)
      // window.location.reload();
    })
      .catch(function (error) {
        console.log(error);
        // console.log(userData.userId)
      });
  }

  async function applyChanges(id) {
    // setIdArray([...idArray, id])
    idArray.push(id);
    console.log(idArray)
    try {
      const response = await axios.post(`${GET_URL()}/api/post/approveAd`, {
        approveAdId: idArray,
        deleteAdId: [],
        reSubmit: true
      });
      console.log(response.data);
      window.location.reload();
    } catch (error) {
      console.log(error.response, "Asd");
    }
  }

  function deleteAd(ad_Id) {
    let userToken = JSON.parse(localStorage.getItem("accessToken"))
    const options = {
      headers: { Authorization: 'Bearer ' + userToken }
    };
    axios.post(`${GET_URL()}/api/post/deleteAd`, {

      userId: userData.userId,
      adId: ad_Id
    }, options
    ).then(function (response) {
      console.log(response.data);
      window.location.reload()
    })
      .catch(function (error) {
        console.log(error.response, "Asd");
      });
  }

  useEffect(() => {

    // userData && setAdProps(userData.userAds);
    userData && getUserAd();
  }, [userData])
  return (
    <>
      <NavBar userData={userData} />
      {/* {console.log(adProps)} */}
      {loading ? <div className="flex justify-center mt-8"><Loader
        type="TailSpin"
        color="#4299e1"
        height={50}
        width={50}
        timeout={3000} //3 secs
      /></div> :
        adProps.length > 0 ?
          <div className="px-10 py-10">
            <table className="table-auto mx-auto">
              <thead>
                <tr>
                  <th className="px-4 py-2">Title</th>
                  <th className="px-4 py-2">Ad Id</th>
                  <th className="px-4 py-2">Status</th>
                </tr>
              </thead>
              <tbody>
                {adProps.map(adProp => {
                  return <Table ad={adProp} applyChanges={applyChanges} deleteAd={deleteAd} />
                })}
              </tbody>
            </table>
          </div>
          :

          <div className="w-full text-center mt-8"><div className=" text-2xl">You have no ads</div>
            <Link to="/postAd"><div className="font-bold mt-2 underline text-blue-500">Create new ad now!</div></Link>
          </div>

      }


    </>
  );
}