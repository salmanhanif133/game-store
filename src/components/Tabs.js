import React from "react";
import {
  Link, useLocation
} from "react-router-dom";
const Tabs = () => {

  return (
    <div className="bg-white flex items-center justify-center border-b border-t">
      <nav className="flex flex-col sm:flex-row">
        <a href="/" className="text-gray-600 py-4 px-6 block hover:text-blue-500 focus:outline-none">
          Home
        </a>
        <a href={`/?name=all&category=Xbox&location=all`}>
          <button className="text-gray-600 py-4 px-6 block hover:text-blue-500 focus:outline-none">
            Xbox
        </button>
        </a>
        <a href={`/?name=all&category=Play station&location=all`}>
          <button className="text-gray-600 py-4 px-6 block hover:text-blue-500 focus:outline-none">
            Play Station
        </button>
        </a>
      </nav>
    </div>
  )
}
export default Tabs;