import React, { useState } from 'react';
import Authentication from './Authentication';
import NavBar from './NavBar';
import { GET_URL } from '../helpers';
const axios = require('axios');


export default function PostAd({ userData }) {
  let userToken = JSON.parse(localStorage.getItem("accessToken"))
  const options = {
    headers: { Authorization: 'Bearer ' + userToken }
  };


  const [image, setImage] = useState("")
  const [previewSource, setPreviewSource] = useState("")
  const uploadImage = (e) => {
    const files = e.target.files
    setImage(files[0])
    const reader = new FileReader();
    reader.readAsDataURL(files[0]);
    reader.onloadend = () => {
      console.log(reader.result);
      setPreviewSource(reader.result)
      setImage(reader.result)
    };
    reader.onerror = () => {
      console.error('AHHHHHHHH!!');
    };
  }


  function addAd(e) {
    e.preventDefault();
    // data.append('userId', userData.userId)
    // data.append('name', name)
    // data.append('price', price)
    // data.append('descriptionShort', descriptionShort)
    // data.append('descriptionLong', descriptionLong)
    // data.append('category', category)
    // data.append('country', country)
    // data.append('province', province)
    // data.append('city', city)
    // data.append('area', area)
    // data.append('condition', condition)
    // data.append('number', phoneNumber)
    axios.post(`${GET_URL()}/api/post/postAd`, {

      name,
      price,
      descriptionShort,
      descriptionLong,
      category,
      country,
      province,
      city,
      area,
      condition,
      number: phoneNumber,
      userId: userData.userId,
      image
    }, options
    ).then(function (response) {
      console.log(response.data);
      setSuccess(response.data.Message);
      setErrors("");
      // window.location.href = "/";
    })
      .catch(function (error) {
        console.log(error.response, "Asd");
        setErrors(error.response.data);
        console.log(userData.userId)
      });
  }


  const [name, setname] = useState("");
  const [price, setprice] = useState("");
  const [descriptionShort, setdescriptionShort] = useState("");
  const [descriptionLong, setdescriptionLong] = useState("");
  const [category, setCategory] = useState("Xbox");
  const [country, setcountry] = useState("");
  const [province, setprovince] = useState("");
  const [city, setcity] = useState("");
  const [area, setarea] = useState("");
  const [condition, setcondition] = useState("Used");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [checkError, setErrors] = useState("");
  const [success, setSuccess] = useState("");
  // let userToken = JSON.parse(localStorage.getItem("accessToken"))
  // let userEmail = JSON.parse(localStorage.getItem("email"))

  return (
    <>

      <NavBar userData={userData} />
      <div className="mx-auto max-w-lg mt-10">

        {userData == null ? <div>Please login or signup to view this page</div> :

          <form className="h-screen" onSubmit={addAd} method="post">
            <p className="text-center mb-4 text-2xl font-bold">Please add details for your Advertisement</p>
            <p className="text-center mb-4 text-base font-bold text-red-500">{checkError.Error}</p>
            <p className="text-center mb-4 text-base font-bold text-green-500">{success}</p>
            <div className="md:flex md:items-center">

              <div className="md:w-1/4">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
                  Name
            </label>
              </div>
              <div className="md:w-2/3">
                <input name="Name" className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" placeholder="Name" type="text" value={name} onChange={(e) => setname(e.target.value)} />
              </div>

            </div>
            <p className="text-center my-2 text-sm text-red-500 font-bold">{checkError.name}</p>
            <div className="md:flex md:items-center">
              <div className="md:w-1/4">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
                  Price
            </label>
              </div>
              <div className="md:w-2/3">
                <input name="Price" className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" type="text" placeholder="Price" value={price} onChange={(e) => setprice(e.target.value)} />
              </div>
            </div>
            <p className="text-center my-2 text-sm text-red-500 font-bold">{checkError.price}</p>
            <div className="md:flex md:items-center">
              <div className="md:w-1/4">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
                  Short Description
            </label>
              </div>
              <div className="md:w-2/3">
                <input name="short description" className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" placeholder=" Short Description" value={descriptionShort} onChange={(e) => setdescriptionShort(e.target.value)} />
              </div>
            </div>
            <p className="text-center my-2 text-sm text-red-500 font-bold">{checkError.descriptionShort}</p>
            <div className="md:flex md:items-center">
              <div className="md:w-1/4">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
                  Long Description
            </label>
              </div>
              <div className="md:w-2/3">
                <input name="Long Description" className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" placeholder="Long Description" value={descriptionLong} onChange={(e) => setdescriptionLong(e.target.value)} />
              </div>
            </div>
            <p className="text-center my-2 text-sm text-red-500 font-bold">{checkError.descriptionLong}</p>
            <div className="md:flex md:items-center">
              <div className="md:w-1/4">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
                  Category
            </label>
              </div>
              <div className="md:w-2/3">
                <select value={category} onChange={(e) => setCategory(e.target.value)} className="py-2 px-2 w-full border-2">
                  <option value="Xbox">Xbox</option>
                  <option value="Play station">Play station</option>
                </select>
                {/* <input className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" placeholder="Category" value={category} onChange={(e) => setcategory(e.target.value)} /> */}
              </div>
            </div>
            <p className="text-center my-2 text-sm text-red-500 font-bold">{checkError.category}</p>
            <div className="md:flex md:items-center">
              <div className="md:w-1/4">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
                  Country
            </label>
              </div>
              <div className="md:w-2/3">
                <input name="Country" className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" placeholder="Country" value={country} onChange={(e) => setcountry(e.target.value)} />
              </div>
            </div>
            <p className="text-center my-2 text-sm text-red-500 font-bold">{checkError.country}</p>
            <div className="md:flex md:items-center">
              <div className="md:w-1/4">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
                  Province
            </label>
              </div>
              <div className="md:w-2/3">
                <input name="Province" className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" placeholder="Province" value={province} onChange={(e) => setprovince(e.target.value)} />
              </div>
            </div>
            <p className="text-center my-2 text-sm text-red-500 font-bold">{checkError.province}</p>
            <div className="md:flex md:items-center">
              <div className="md:w-1/4">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
                  City
            </label>
              </div>
              <div className="md:w-2/3">
                <input name="City" className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" placeholder="City" value={city} onChange={(e) => setcity(e.target.value)} />
              </div>
            </div>
            <p className="text-center my-2 text-sm text-red-500 font-bold">{checkError.city}</p>
            <div className="md:flex md:items-center">
              <div className="md:w-1/4">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
                  Area
            </label>
              </div>
              <div className="md:w-2/3">
                <input name="Area" className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" placeholder="Area" value={area} onChange={(e) => setarea(e.target.value)} />
              </div>
            </div>
            <p className="text-center my-2 text-sm text-red-500 font-bold">{checkError.area}</p>

            <div className="md:flex md:items-center">
              <div className="md:w-1/4">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
                  Image
            </label>
              </div>
              <div className="md:w-2/3">
                <input type="file" name="file" className="mt-4" placeholder="upload image" onChange={uploadImage} />
              </div>
            </div>
            <div>
              {previewSource && (
                <img
                  src={previewSource}
                  alt="chosen"
                  style={{ height: '300px' }}
                />
              )}
            </div>
            <p className="text-center my-2 text-sm text-red-500 font-bold">{checkError.number}</p>

            <div className="md:flex md:items-center">
              <div className="md:w-1/4">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
                  Phone Number
            </label>
              </div>
              <div className="md:w-2/3">
                <input name="Number" className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" placeholder="Phone Number" value={phoneNumber} onChange={(e) => setPhoneNumber(e.target.value)} />
              </div>
            </div>
            <p className="text-center my-2 text-sm text-red-500 font-bold">{checkError.number}</p>

            <div className="md:flex md:items-center">
              <div className="md:w-1/4">
                <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
                  Condition
            </label>
              </div>
              <div className="md:w-2/3">
                <select value={condition} onChange={(e) => setcondition(e.target.value)} className="py-2 px-2 w-full border-2">
                  <option value="Used">Used</option>
                  <option value="Unused">Unused</option>
                </select>
                {/* <input className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" placeholder="Condition" value={condition} onChange={(e) => setcondition(e.target.value)} /> */}
              </div>
            </div>
            {/* <p className="text-center my-2 text-sm text-red-500 font-bold">{checkError.email}</p> */}
            {/* <p className="text-center my-2 text-sm text-red-500 font-bold">{checkError.username}</p> */}
            <div className="md:flex md:items-center justify-center">
              {/* <Link href="/"> */}
              <button type="submit" className="shadow bg-green-500 hover:bg-green-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" >
                Post
            </button>
              {/* </Link> */}
            </div>
          </form>
        }
      </div>
    </>
  )
}
