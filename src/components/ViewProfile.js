import React, { useEffect, useState } from "react";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from "react-loader-spinner";
import { Link, useParams } from "react-router-dom";
import NavBar from "./NavBar";
// import adLogo from '../fifa20.jpg'
import Authentication from "./Authentication";
import { GET_URL } from "../helpers";
const axios = require("axios");

const Table = ({ ad, deleteAd, applyChanges }) => {
  return (
    <tr>
      <td className="border px-4 py-2 w-64 text-center font-bold text-blue-500 ">
        {" "}
        <Link to={`/ads/${ad._id}`} className="no-underline hover:underline">
          {ad.name}
        </Link>
      </td>
      <td className="border border-r-0 px-4 py-2 w-64 text-center">{ad._id}</td>
    </tr>
  );
};

export default function ViewProfile({ userData }) {
  const [userData2, setUserData2] = useState([]);
  const [adProps, setAdProps] = useState([]);
  const [loading, setLoading] = useState(true);
  let { id } = useParams();
  let avatar;
  try {
    avatar = userData2.imagePath
  } catch (e) {
    console.log(e);
    avatar = false;
  }

  function getUserAd(e) {
    axios
      .post(`${GET_URL()}/api/getUserAd`, {
        userId: id,
      })
      .then(function (response) {
        setAdProps(response.data.result);
        setLoading(false);
        // window.location.reload();
      })
      .catch(function (error) {
        console.log(error);
        // console.log(userData.userId)
      });
  }

  async function getUserById() {
    try {
      const response = await axios.get(`${GET_URL()}/api/profile/${id}`);
      setUserData2(response.data);
      setLoading(false);
      console.log(response.data);
    } catch (error) {
      console.error(error);
    }
  }

  useEffect(() => {
    // userData && setAdProps(userData.userAds);
    getUserById();
    getUserAd();
  }, []);
  return (
    <>
      <NavBar userData={userData} />

      <div className="px-10 py-10 flex">
        <div className="w-1/6 p-2 text-center">
          {avatar ? (
            <img
              className="w-32 h-32 rounded-full mx-auto"
              src={userData2.imagePath}
            />
          ) : (
              <img
                className="w-32 h-32 rounded-full mx-auto"
                src="https://res.cloudinary.com/salman9000/image/upload/v1594594074/profileimage/profilepicplaceholder_ej0ow2.png"
              />
            )}
          <p className="mt-2 text-xl">User Profile</p>
        </div>
        <div className="w-5/6 px-8 py-4">
          <div className="pb-4 flex border-b border-gray-300">
            <p className="text-5xl leading-7 text-gray-900 mr-8">
              {userData2.firstname} {userData2.lastname}
            </p>
          </div>
          {loading ? (
            <div className="flex justify-center mt-8">
              <Loader
                type="TailSpin"
                color="#4299e1"
                height={50}
                width={50}
                timeout={3000} //3 secs
              />
            </div>
          ) : adProps.length > 0 ? (
            <>
              <p className="text-2xl leading-loose text-gray-900 mb-2 font-bold">
                User advertisements
              </p>
              <table className="table-auto mx-auto w-full px-4">
                <thead>
                  <tr>
                    <th className="px-4 py-2">Title</th>
                    <th className="px-4 py-2">Ad Id</th>
                  </tr>
                </thead>
                <tbody>
                  {adProps.map((adProp) => {
                    return <Table ad={adProp} />;
                  })}
                </tbody>
              </table>
            </>
          ) : (
                <div className="w-full text-center mt-8">
                  <div className=" text-2xl">User has no ads</div>
                </div>
              )}
        </div>
      </div>
    </>
  );
}
