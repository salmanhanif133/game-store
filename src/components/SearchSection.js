import Posts from "./Advertisements";
require('es6-promise').polyfill();
require('isomorphic-fetch');
import Link from 'next/link';
import { GET_URL } from "../helpers";

const SearchSection = ({ data }) => {
  // const adProps = {
  //   ad: [
  //     { id: 0, name: "Fifa20", price: "4000", desc: "Awesome game", cat: "Play station 4", location: "Karachi", time: "today", isFeatured: true },
  //     { id: 1, name: "Fifa 19", price: "2000", desc: "Nice game", cat: "Play station 3", location: "Karachi", time: "today", isFeatured: false },
  //     { id: 2, name: "PES", price: "3000", desc: "great game", cat: "Play station 2", location: "Karachi", time: "today", isFeatured: true },
  //     { id: 3, name: "Cricket", price: "1000", desc: "slightly used game", cat: "Xbox 360", location: "Karachi", time: "today", isFeatured: true },
  //     { id: 4, name: "Naruto", price: "1500", desc: "brand new game", cat: "Xbox one", location: "Karachi", time: "today", isFeatured: false },
  //     { id: 5, name: "SOmething", price: "2500", desc: "i dont like this game", cat: "Xbox one s", location: "Karachi", time: "today", isFeatured: false }
  //   ]
  // }
  return (
    <div className="bg-gray-200 px-8 flex flex-wrap -ml-8 ">
      <p className="text-4xl font-bold mb-4 w-full pl-16">Recommended: </p>
      {data && data.map((ads) => {
        return <Link href="/p/[id]" as={`/p/${ads.id}-${ads.name}`}>
          <a><Posts ads={ad} /></a>
        </Link>
      })}
    </div>
  )
}

SearchSection.getInitialProps = async () => {
  const rawData = await fetch(`${GET_URL()}/api/ad`);
  const data = await rawData.json();
  return { data };

}

export default SearchSection;