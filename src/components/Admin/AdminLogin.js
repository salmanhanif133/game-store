import React, { useState } from 'react';
import { GET_URL } from '../helpers';
const axios = require('axios');
export default function Login() {

  function loginUser(e) {
    e.preventDefault();
    axios.post(`${GET_URL()}/api/login`, {
      email,
      password
    }).then(function (response) {
      console.log(response);
      localStorage.setItem("email", JSON.stringify(response.data.email));
      localStorage.setItem("accessToken", JSON.stringify(response.data.accessToken));
      window.location.href = "/";
    })
      .catch(function (error) {
        console.log(error.response);
        setErrors(error.response.data);
      });
  }

  const [checkError, setErrors] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  return (
    <div className="mx-auto max-w-lg mt-10">

      <form className="h-screen" onSubmit={loginUser}>
        <p className="text-center mb-4 text-base font-bold text-red-500">{checkError.Error}</p>


        <div className="md:flex md:items-center mb-6">
          <div className="md:w-1/4">
            <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
              Email
      </label>
          </div>
          <div className="md:w-2/3">
            <input name="Email" className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-blue-500" placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)} />
          </div>
        </div>
        <p className="text-center my-2 text-sm text-red-500 font-bold">{checkError.email}</p>
        <div className="md:flex md:items-center mb-6">
          <div className="md:w-1/4">
            <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
              Password
      </label>
          </div>
          <div className="md:w-2/3">
            <input name="Password" className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-blue-500" type="password" placeholder="**********" value={password} onChange={(e) => setPassword(e.target.value)} />
          </div>
        </div>
        <p className="text-center my-2 text-sm text-red-500 font-bold">{checkError.password}</p>
        <div className="md:flex md:items-center justify-center">
          <button type="submit" className="shadow bg-blue-500 hover:bg-blue-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" >
            Log In
      </button>
        </div>
      </form>
    </div>
  )
}
