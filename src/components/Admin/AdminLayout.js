import React from "react";
import { Link } from "react-router-dom";
import navLogo from '../../LOGO.png'
const NavLink = () => {
  return (
    <ul>
      <li className="uppercase block sm:inline-block sm:mt-0 text-white p-4 mr-1 text-center"
      >
        <Link to="/postAd">
          Sell
        </Link>
      </li>
      <li className="uppercase block sm:inline-block sm:mt-0 text-white p-4 mr-1 text-center"
      >
        <Link to="/postAd">
          Profile
        </Link>
      </li>
      <li className="uppercase block sm:inline-block sm:mt-0 text-white p-4 mr-1 text-center"
      >
        <Link to="/myads">
          View your ads
        </Link>
      </li>

    </ul>
  );
};

const AdminLayout = () => {

  return (
    <>
      <div className="flex">

        <nav className="w-64 bg-gray-900 pt-16 px-8 fixed top-0 bottom-0">
          <Link to="/admin/dashboard" className="block text-white py-2 px-8 hover:text-gray-500 hover:bg-gray-800">Dashboard</Link>
          <Link to="/admin/advertisements" className="block text-white py-2 px-8 hover:text-gray-500 hover:bg-gray-800">Advertisements</Link>
          <Link to="/admin/users" className="block text-white py-2 px-8 hover:text-gray-500 hover:bg-gray-800">Users</Link>
          <Link to="/" className="block text-white py-2 px-8 hover:text-gray-500 hover:bg-gray-800">Item</Link>
          <Link to="/" className="block text-white py-2 px-8 hover:text-gray-500 hover:bg-gray-800">Item</Link>
        </nav >

        <div className="w-full ml-64">
          <nav className="flex items-center justify-between flex-wrap bg-blue-500">
            <div className="flex items-center flex-shrink-0 text-white mr-6 p-2 pl-4">
              <Link to="/">
                <img
                  className="mr-2 h-10"
                  src={navLogo}
                  alt="Logo"
                />

              </Link>
            </div>
            <div
              className={`w-full flex-grow sm:flex sm:items-center sm:w-auto`}>
              <div className="text-sm sm:flex-grow sm:flex sm:justify-center">
                <NavLink />
              </div>
            </div>
          </nav>
        </div>
      </div>


    </>
  );
}

export default AdminLayout;
