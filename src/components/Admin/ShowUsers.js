import React, { useEffect, useState } from "react";
import AdminLayout from './AdminLayout';
import { GET_URL } from "../../helpers";
const axios = require('axios');

const ShowUsers = () => {

  // let userToken = JSON.parse(localStorage.getItem("accessToken"))
  const [adData, setAdData] = useState({ ads: [] });
  const [idArray, setIdArray] = useState([]);
  const [discardidArray, setdiscardIdArray] = useState([]);
  const [unbanArray, setunbanArray] = useState([]);
  const [loading, setLoading] = useState(true);
  const [checkError, setErrors] = useState("");
  const [success, setSuccess] = useState("");

  const discardChanges = () => {
    setdiscardIdArray([])
    setIdArray([])
    console.log(idArray, discardidArray)
    setAdData({
      ads: adData.ads.map(ad => {
        ad.deleted = false;
        return ad;
      })
    })
    setAdData({
      ads: adData.ads.map(ad => {
        ad.reject = false;
        return ad;
      })
    })
  }

  async function applyChanges() {
    try {
      const response = await axios.post(`${GET_URL()}/api/post/deleteUser`, {
        deleteAdId: idArray,
        disableId: discardidArray,
        unbanId: unbanArray
      });
      console.log(response.data);
      setSuccess(response.data.Message);
      setErrors("");
      window.location.reload();
    } catch (error) {
      console.log(error.response, "Asd");
      setErrors(error.response.data);
    }
  }

  async function getAllUser() {
    try {
      console.log(process.env)
      const response = await axios.get(`${GET_URL()}/api/users/`);
      setAdData({ ads: response.data.result })
      setLoading(false);
      console.log(response.data)
    } catch (error) {
      console.error(error);
    }
  }

  const deleteAd = (id) => { //Delete
    setIdArray([...idArray, id])
    console.log(idArray)
    setAdData({
      ads: adData.ads.map(ad => {
        if (ad._id === id) {
          ad.deleted = true;
        }
        return ad;
      })
    })
  }
  const undoDeleteAd = (id) => { //undodelete
    setIdArray(idArray.filter(adId => {
      if (adId !== id) {
        return adId;
      }
    }))
    console.log(idArray)
    setAdData({
      ads: adData.ads.map(ad => {
        if (ad._id === id) {
          ad.deleted = false;
        }

        return ad;
      })
    })
  }
  const rejectAd = (id) => { //reject
    setdiscardIdArray([...discardidArray, id])
    console.log(discardidArray)
    setAdData({
      ads: adData.ads.map(ad => {
        if (ad._id === id) {
          ad.reject = true;
        }
        return ad;
      })
    })
  }

  const undorejectAd = (id) => { //undo reject
    setdiscardIdArray(discardidArray.filter(adId => {
      if (adId !== id) {
        return adId;
      }
    }))
    console.log(discardidArray)
    setAdData({
      ads: adData.ads.map(ad => {
        if (ad._id === id) {
          ad.reject = false;
        }

        return ad;
      })
    })
  }

  const unBanUser = (id) => { //Unban
    setunbanArray([...unbanArray, id])
    console.log(unbanArray)
    setAdData({
      ads: adData.ads.map(ad => {
        if (ad._id === id) {
          ad.ban = true;
        }
        return ad;
      })
    })
  }

  const undounBanUser = (id) => { //undo reject
    setunbanArray(unbanArray.filter(adId => {
      if (adId !== id) {
        return adId;
      }
    }))
    console.log(unbanArray)
    setAdData({
      ads: adData.ads.map(ad => {
        if (ad._id === id) {
          ad.ban = false;
        }

        return ad;
      })
    })
  }


  useEffect(() => {
    getAllUser()
  }, [])
  console.log(adData)
  return (
    <div className="bg-gray-200">
      <AdminLayout />
      <div className="ml-64 px-10 py-6">
        <p className="font-bold text-2xl mb-2">Users</p>
        <div className="bg-white border border-gray-400 rounded-lg">
          <div className="p-8">
            <div>
              <p className="text-xl mb-8">Users</p>
              {/* Table start */}
              <table className="table-fixed">
                <thead>
                  <tr>
                    <th className="w-1/12 pb-4 text-left text-xs tracking-widest font-normal uppercase text-gray-500">#</th>
                    <th className="w-3/12 pb-4 text-left text-xs tracking-widest font-normal uppercase text-gray-500">ID</th>
                    <th className="w-2/12 pb-4 text-left text-xs tracking-widest font-normal uppercase text-gray-500">Name</th>
                    <th className="w-2/12 pb-4 text-left text-xs tracking-widest font-normal uppercase text-gray-500">Username</th>
                    <th className="w-2/12 pb-4 text-left text-xs tracking-widest font-normal uppercase text-gray-500">Status</th>
                    <th className="w-2/12 pb-4 text-left text-xs tracking-widest font-normal uppercase text-gray-500">Ban</th>
                    <th className="w-2/12 pb-4 text-left text-xs tracking-widest font-normal uppercase text-gray-500">Delete</th>
                  </tr>
                </thead>
                <tbody>
                  {loading ? <tr><td>Loading</td></tr> : adData.ads.map((ad, i) => <AdRow index={i} key={ad._id} {...ad} deleteAd={deleteAd} undoDeleteAd={undoDeleteAd} rejectAd={rejectAd} undorejectAd={undorejectAd} unBanUser={unBanUser} undounBanUser={undounBanUser} />)}
                </tbody>
              </table>
              {!loading && adData.ads.length < 1 ? <div className="text-center">No pending Advertisements</div> :
                <div className="text-center mt-6">
                  <button className="bg-white border border-gray-400 hover:bg-blue-600 hover:text-white text-blue-600 font-semibold tracking-widest text-sm py-1 px-6 rounded mr-10" onClick={() => applyChanges()}>Apply</button>
                  <button className="bg-white border border-gray-400 hover:bg-red-600 hover:text-white text-red-600 font-semibold tracking-widest text-sm py-1 px-6 rounded" onClick={() => discardChanges()}>Discard</button>
                </div>
              }
            </div>
          </div>

        </div>
      </div>
    </div>
  )
};

const AdRow = ({ _id, firstname, lastname, username, deleted, deleteAd, undoDeleteAd, reject, rejectAd, undorejectAd, index, isBan, unBanUser, undounBanUser, ban }) => {
  let Banned;
  if (isBan == "true") {
    // ban = true;
    Banned = "Banned"
  } else {
    Banned = "Active"
    // ban = false;
  }
  console.log(ban)
  return (<>
    {/* when delete button is active when remove is true*/}
    {reject ? <tr className="bg-orange-200">
      <td className="text-sm py-2">{index + 1}</td>
      <td className="text-red-700 text-sm py-2">{_id}</td>
      <td className="text-red-700 py-4">{firstname} {lastname}</td>
      <td className="text-red-700 py-4">{username}</td>
      <td className="text-red-700 py-4">{Banned}</td>
      <td className="">
        <button className="bg-orange-600 focus:outline-none hover:bg-orange-800 text-white text-sm py-1 px-4 rounded" onClick={() => undorejectAd(_id)}>Unban</button>
      </td>
      <td className="">
        <button className="bg-red-300 focus:outline-none cursor-not-allowed text-white text-sm py-1 px-4 rounded">
          Delete
      </button>
      </td>
    </tr> :
      // when delete button is not active
      !deleted ? //Default view
        <tr>
          <td className="text-sm py-2">{index + 1}</td>
          <td className="text-blue-700 text-sm py-2">{_id}</td>
          <td className="text-gray-700 py-4">{firstname} {lastname}</td>
          <td className="text-blue-700 py-4">{username}</td>
          {isBan === "true" ?

            <>
              {!ban ?
                <>
                  <td className="text-red-500 font-bold py-4 ">{Banned}</td>
                  <td className="">
                    <button className="bg-green-500 hover:bg-green-600 focus:outline-none text-white text-sm py-1 px-4 rounded"
                      onClick={() => unBanUser(_id)}>
                      Unban
            </button>
                  </td>
                </> :
                <>
                  <td className="text-green-500 font-bold py-4 bg-green-200">Active</td>
                  <td className="">
                    <button className="bg-blue-500 hover:bg-blue-600 focus:outline-none text-white text-sm py-1 px-4 rounded"
                      onClick={() => undounBanUser(_id)}>
                      Ban
         </button>
                  </td>
                </>
              }

            </> :
            <>
              <td className="font-bold text-green-500 py-4">{Banned}</td>
              <td className="">
                <button className="bg-blue-600 hover:bg-red-700 text-white text-sm py-1 px-4 rounded"
                  onClick={() => rejectAd(_id)}>
                  Ban
      </button>
              </td>
            </>}
          <td className="">
            {/* when it is not approved  approved is false*/}
            <button className="bg-red-600 hover:bg-red-700 text-white text-sm py-1 px-4 rounded"
              onClick={() => {
                deleteAd(_id)
              }}>
              Delete
      </button>
          </td>
        </tr> :
        <tr className="bg-red-200">
          <td className="text-sm py-2">{index + 1}</td>
          <td className="text-red-700 text-sm py-2">{_id}</td>
          <td className="text-red-700 py-4">{firstname} {lastname}</td>
          <td className="text-red-700 py-4">{username}</td>
          <td className="text-red-700 py-4">{Banned}</td>
          <td className="">
            <button className="bg-red-300 focus:outline-none cursor-not-allowed text-white text-sm py-1 px-4 rounded"
            >
              Ban
      </button>
          </td>
          <td className="">
            {/* when it is not approved  approved is false*/}
            <button className="bg-blue-500 hover:bg-blue-700 focus:outline-none border-b-2 border-blue-600 text-white text-sm py-1 px-4 rounded"
              onClick={() => undoDeleteAd(_id)}>
              Undo
      </button>
          </td>
        </tr>

    }
  </>)
}

export default ShowUsers;