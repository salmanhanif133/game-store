import React, { useEffect, useState } from "react";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from "react-loader-spinner";
import { Link, useParams } from "react-router-dom";
import NavBar from "./NavBar";
// import adLogo from '../fifa20.jpg'
import Authentication from "./Authentication";
import { GET_URL } from "../helpers";
const axios = require("axios");

const EditForm = ({ userData }) => {
  const [userName, setuserName] = useState(
    `${userData.firstname} ${userData.lastname}`
  );
  const [email, setemail] = useState(`${userData.email}`);
  const [phonenumber, setphonenumber] = useState(`${userData.phonenumber}`);
  const [aboutme, setaboutme] = useState(`${userData.aboutme}`);
  const [checkError, setErrors] = useState("");
  const [success, setSuccess] = useState("");

  let userToken = JSON.parse(localStorage.getItem("accessToken"));
  const options = {
    headers: { Authorization: "Bearer " + userToken },
  };
  function applyChanges(e) {
    // setCheckErrors(true);
    e.preventDefault();
    const firstname = userName.split(" ")[0];
    const lastname = userName.split(" ")[1];
    console.log(userName);
    console.log(phonenumber);
    console.log(aboutme);
    console.log(firstname);
    console.log(lastname);
    axios
      .post(
        `${GET_URL()}/api/updateUser`,
        {
          firstname,
          lastname,
          email,
          aboutme,
          phonenumber,
          userId: userData.userId,
        },
        options
      )
      .then(function (response) {
        console.log(response.data);
        setSuccess(response.data.Message);
        setErrors("");
        localStorage.setItem("email", JSON.stringify(email));
        window.location.reload();
      })
      .catch(function (error) {
        console.log(error.response, "Asd");
        setErrors(error.response.data);
        console.log(userData.userId);
      });
  }

  const discardChanges = () => {
    setuserName(`${userData.firstname} ${userData.lastname}`);
    setemail(`${userData.email}`);
    setphonenumber(`${userData.phonenumber}`);
    setaboutme(`${userData.aboutme}`);
  };

  useEffect(() => { }, [success]);
  return (
    <div className="mb-8">
      <div className="border">
        <p className="text-xl text-gray-900 mb-2 font-bold px-10 border-b py-4">
          Edit Profile
        </p>
        <form className="px-10" onSubmit={applyChanges}>
          <p className="text-left mb-4 text-xl font-bold">Basic Information</p>
          <div className="pb-4 border-b mb-4">
            <div className="md:items-center">
              <label className="block text-gray-800 font-bold  mb-1 md:mb-1 pr-4">
                Your Name
              </label>
              <input
                name="Name"
                className="bg-white appearance-none border mb-4 border-gray-700 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-blue-500"
                placeholder="Name"
                type="text"
                value={userName}
                onChange={(e) => setuserName(e.target.value)}
              />
            </div>
            <div className="md:items-center">
              <label className="block text-gray-800 font-bold  mb-1 md:mb-1 pr-4">
                About me
              </label>
              <input
                name="aboutme"
                className="bg-white appearance-none border mb-4 border-gray-700 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-blue-500"
                placeholder="About me (Optional)"
                type="text"
                value={aboutme}
                onChange={(e) => setaboutme(e.target.value)}
              />
            </div>
          </div>

          <p className="text-left mb-4 text-xl font-bold">
            Contact Information
          </p>
          <div className="pb-4 border-b mb-4">
            <div className="md:items-center">
              <label className="block text-gray-800 font-bold  mb-1 md:mb-1 pr-4">
                Your Phone
              </label>
              <input
                name="phonenumber"
                className="bg-white appearance-none border mb-4 border-gray-700 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-blue-500"
                placeholder="phonenumber"
                type="text"
                value={phonenumber}
                onChange={(e) => setphonenumber(e.target.value)}
              />
            </div>
            {/* <p className="text-center my-2 text-sm text-red-500 font-bold">{checkError.name}</p> */}
            <div className="md:items-center">
              <label className="block text-gray-800 font-bold  mb-1 md:mb-1 pr-4">
                Your Email
              </label>
              <input
                name="email"
                className="bg-white appearance-none border mb-4 border-gray-700 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-blue-500"
                placeholder="email"
                type="text"
                value={email}
                onChange={(e) => setemail(e.target.value)}
              />
            </div>
            <p className="text-red-500 font-bold text-base">
              Note: You will have to login again if you change your email
            </p>
          </div>

          {/* <p className="text-center my-2 text-sm text-red-500 font-bold">{checkError.price}</p> */}
          <div className="md:flex md:items-center justify-between my-4">
            {/* <Link href="/"> */}
            <button
              type="reset"
              className="bg-white border border-red-400 hover:bg-red-600 hover:text-white text-red-600 font-semibold tracking-widest text-sm py-2 px-6 rounded mr-10"
              onClick={discardChanges}
            >
              Discard
            </button>
            <button
              type="submit"
              className="bg-white border border-blue-400 hover:bg-blue-600 hover:text-white text-blue-600 font-semibold tracking-widest text-sm py-2 px-6 rounded mr-10"
            >
              Save Changes
            </button>

            {/* </Link> */}
          </div>
        </form>
      </div>
    </div>
  );
};

export default function EditProfile({ userData }) {
  let userToken = JSON.parse(localStorage.getItem("accessToken"));
  const options = {
    headers: { Authorization: "Bearer " + userToken },
  };
  let avatar;
  try {
    avatar = userData.imagePath
  } catch (e) {
    console.log(e);
    avatar = false;
  }
  const [loading, setLoading] = useState(false);
  const [userName, setuserName] = useState(
    `${userData.firstname} ${userData.lastname}`
  );

  const [image, setImage] = useState("")
  const [previewSource, setPreviewSource] = useState("")
  const [imgLoad, setImgload] = useState(false)

  const uploadImage = (e) => {
    const files = e.target.files
    const reader = new FileReader();
    reader.readAsDataURL(files[0]);
    reader.onloadend = () => {
      console.log(reader.result);
      setPreviewSource(reader.result)
      setImage(reader.result)
    };
    reader.onerror = () => {
      console.error('AHHHHHHHH!!');
    };
  }


  const onClickHandler = async (e) => {
    setImgload("uploading")
    try {
      const response = await axios.post(`${GET_URL()}/api/uploadImage`, {
        userId: userData.userId,
        image
      }, options);
      console.log(response.statusText);
      setImgload(true)
      window.location.reload();
    } catch (error) {
      console.log(error, "Asd");
    }
  };

  return (
    <>
      <NavBar userData={userData} />
      {loading ? (
        <div className="flex justify-center mt-8">
          <Loader
            type="TailSpin"
            color="#4299e1"
            height={50}
            width={50}
            timeout={3000} //3 secs
          />
        </div>
      ) : (
          <div className="px-10 py-10 flex">
            <div className="w-1/6 p-2 text-center">
              {avatar ? (
                <img
                  className="w-32 h-32 rounded-full mx-auto"
                  src={userData.imagePath}
                />
              ) : (
                  <img
                    className="w-32 h-32 rounded-full mx-auto"
                    src="https://res.cloudinary.com/salman9000/image/upload/v1594594074/profileimage/profilepicplaceholder_ej0ow2.png"
                  />
                )}
              <p className="mt-2 text-xl">Change profile picture</p>
              <div className="my-4">
                {previewSource && (
                  <img
                    className="h-40 mx-auto"
                    src={previewSource}
                    alt="chosen"
                  />
                )}
                {imgLoad == "uploading" && <p>Uploading</p>}
              </div>
              <input
                type="file"
                name="file"
                className="mt-4"
                placeholder="upload image"
                onChange={uploadImage}
              />
              <button
                type="button"
                className="bg-blue-500 px-4 py-2 text-white rounded-lg mt-4 "
                onClick={onClickHandler}
              >
                Upload
            </button>
              <Link to="/profile">
                <button className="mt-4 text-blue-500 text-xl border-2 border-blue-500 px-4 py-1 bg-white hover:bg-blue-300">
                  View profile
              </button>
              </Link>
            </div>
            <div className="w-5/6 px-8 py-4">
              <div className="pb-4 mb-8 flex border-b border-gray-300">
                <p className="text-5xl leading-7 text-gray-900 mr-8">
                  {userName}
                </p>
              </div>
              <EditForm userData={userData} setuserName={setuserName} />
            </div>
          </div>
        )}
    </>
  );
}
