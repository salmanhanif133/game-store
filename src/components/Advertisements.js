import React from "react";
import adLogo from '../fifa20.jpg'
const Posts = ({ ad, userId }) => {
  if (userId != null) {
    userId = userId.userId
  }
  let featureBox;
  let featureBorder;
  let greenBorder = "";
  if (ad.isFeatured) {
    featureBox = <div className="w-1/3 bg-yellow-400 h-5 absolute text-xs uppercase font-bold text-center top-0 mt-4">Featured</div>
    featureBorder = 'border-b-4 border-yellow-400';
  }
  if (userId && ad.createdBy === userId) { greenBorder = " border-4 border-green-500" }


  return (

    <div className={`w-64 rounded overflow-hidden shadow-lg bg-white mb-8 ${greenBorder}`}>
      <div className="flex mb-4 relative h-40">
        {featureBox}
        <img className="mx-auto" src={ad.adImage.replace("upload/", "upload/c_fill,h_150/")} alt="Sunset in the mountains" />
      </div>
      <div className="px-4 mb-2">
        <div className="font-bold text-xl uppercase">{ad.name}</div>
        <div className="font-bold text-xl mb-2">Rs {ad.price}</div>
        <p className="text-gray-700 text-base">{ad.description.short}</p>
      </div>
      <div className="px-4 mb-4">
        <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2">{ad.category}</span>
      </div>
      <div className={`flex pb-4 justify-between text-xs px-4 uppercase text-gray-700 ${featureBorder}`}>
        <p>{ad.location.city}</p>
        <p>{ad.time}</p>

      </div>
    </div>
  )
}
export default Posts;