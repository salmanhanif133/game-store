import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import navLogo from "../LOGO.png";
import { GET_URL } from "../helpers";
const axios = require("axios");
const NavLink = () => {
  return (
    <ul>
      <li className="uppercase block sm:inline-block sm:mt-0 text-white p-4 mr-1 text-center">
        <Link to="/postAd">Sell</Link>
      </li>
      <li className="uppercase block sm:inline-block sm:mt-0 text-white p-4 mr-1 text-center">
        <Link to="/profile">Profile</Link>
      </li>
      <li className="uppercase block sm:inline-block sm:mt-0 text-white p-4 mr-1 text-center">
        <Link to="/myads">View your ads</Link>
      </li>
    </ul>
  );
};

function NavBar({ userData }) {
  const [open, setOpen] = React.useState(false);
  const [loggedin, SetLoggedIn] = React.useState(false);
  let show = "";

  function handleClick() {
    setOpen(!open);
  }

  const logout = async (email) => {
    try {
      const response = await axios.delete(`${GET_URL()}/api/logout`, {
        data: { email: email },
      });
      console.log(response);
      localStorage.removeItem("email");
      localStorage.removeItem("accessToken");
      window.location.href = "/";
    } catch (error) {
      console.error(error);
    }
  };
  show = open ? "block" : "hidden";
  useEffect(() => {
    userData && SetLoggedIn(true);
  }, [userData]);
  return (
    <nav className="flex items-center justify-between flex-wrap bg-blue-500">
      <div className="flex items-center flex-shrink-0 text-white mr-6 p-2 pl-4">
        <Link to="/">
          <img className="mr-2 h-10" src={navLogo} alt="Logo" />
        </Link>
      </div>
      <div className="block sm:hidden">
        <button
          className="fill-current flex items-center px-3 py-2 border rounded text-white border-white hover:text-white hover:border-white mr-4"
          onClick={handleClick}
        >
          <svg
            className="h-3 w-3"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Menu</title>
            <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
          </svg>
        </button>
      </div>

      <div
        className={`w-full ${show} flex-grow sm:flex sm:items-center sm:w-auto`}
      >
        <div className="text-sm sm:flex-grow sm:flex sm:justify-center">
          <NavLink />
        </div>
        {loggedin ? (
          <div className="flex">
            <div className="text-center">
              <div className="uppercase inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white  sm:mr-4 sm:m-0 m-4">
                Logged in as: {userData.firstname}
              </div>
            </div>
            <div className="text-center">
              <button
                onClick={() => logout(userData.email)}
                className="bg-blue-700 uppercase inline-block text-sm px-4 py-2 leading-none border rounded text-white border-blue-700 hover:border-transparent hover:text-blue-500 hover:bg-white sm:mr-4 sm:m-0 m-4"
              >
                logout
              </button>
            </div>
          </div>
        ) : (
          <>
            <div className="text-center">
              <Link
                to="/login"
                className="uppercase inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-blue-500 hover:bg-white sm:mr-4 sm:m-0 m-4"
              >
                Log in
              </Link>
            </div>
            <div className="text-center">
              <Link
                to="signin"
                className="bg-blue-700 uppercase inline-block text-sm px-4 py-2 leading-none border rounded text-white border-blue-700 hover:border-transparent hover:text-blue-500 hover:bg-white sm:mr-4 sm:m-0 m-4"
              >
                Sign up
              </Link>
            </div>
          </>
        )}
      </div>
    </nav>
  );
}

export default NavBar;
