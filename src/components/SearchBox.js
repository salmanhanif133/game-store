import React from "react";
const SearchBox = () => {
  const [category, setCategory] = React.useState("all");
  const [name, setName] = React.useState("");
  const [location, setLocation] = React.useState("all");
  const [search, setSearch] = React.useState("all")

  return (
    <div
      className="overflow-hidden flex items-center justify-center py-8 px-10"
    >
      <div className="container flex flex-row">
        <select value={category} onChange={(e) => setCategory(e.target.value)} className="pl-4 pr-10 w-1/3 border-2 border-r-0">
          <option value="0">All Categories</option>
          <option value="Xbox">Xbox</option>
          <option value="Play station">Play station</option>
        </select>
        <input
          className="w-full h-12  text-xl px-4 border-2 border-r-0 appearance-none"
          type="search"
          placeholder="Search..."
          type="text" value={name} onChange={(e) => setName(e.target.value)} />
        <input
          className="h-12  text-xl px-4 border-2 appearance-none"
          placeholder="Location"
        />

        <a href={`/?name=${name ? name : search}&category=${category}&location=${location}`} className="bg-blue-400 rounded-lg px-5 ml-4 text-white text-xl hover:bg-blue-500 text-center py-2">Search</a>

      </div>
    </div >
  )
}
export default SearchBox;