import React from "react";
import { GET_URL } from "../helpers";

const axios = require('axios');

const Authentication = async () => {
  const [userData, setUserData] = React.useState(null);
  let userToken = JSON.parse(localStorage.getItem("accessToken"))
  let userEmail = JSON.parse(localStorage.getItem("email"))

  userToken = JSON.parse(localStorage.getItem("accessToken"))
  try {
    const response = await axios.get(`${GET_URL()}/api/getUser`, {
      headers: { Authorization: 'Bearer ' + userToken }
    });
    console.log(response, " get user response");
    setUserData(response.data)
  } catch (error) {
    console.error(error, "Access token expired");
    // refreshToken(userEmail);
  }


  // async function refreshToken(userEmail) {
  //   try {
  //     const response = await axios.post(`${GET_URL()}/api/token`, {
  //       email: userEmail
  //     })
  //     console.log(response, "get refrestoken");
  //     localStorage.setItem("accessToken", JSON.stringify(response.data.accessToken));

  //     getUser()
  //   } catch (error) {
  //     console.error(error);
  //     setUserData(null)
  //   }
  // }
  return userData;
}
export default Authentication;
