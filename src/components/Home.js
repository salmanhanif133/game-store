import React, { useState, useEffect } from "react";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import Loader from 'react-loader-spinner'
import Layout from "./Layout";
import Tabs from "./Tabs";
import Posts from "./Advertisements";
import '../styles/app.css'
import {
  Link, useLocation
} from "react-router-dom";
import { GET_URL } from "../helpers";

const axios = require('axios');

export default function Home({ userData }) {
  const [adData, setAdData] = useState([]);
  const [loading, setLoading] = useState(true);
  // const [userData, setUserData] = React.useState(null);
  // let userToken = JSON.parse(localStorage.getItem("accessToken"))
  // let userEmail = JSON.parse(localStorage.getItem("email"))

  let query = new URLSearchParams(useLocation().search);
  // console.log(query.get("category"), "category")
  // console.log(query.get("name"), "name")
  // console.log(query.get("location"), "location")
  // const authenticateUser = (data) => {
  //   setUserData(data)
  // }
  // async function getAllAds() {
  //   try {
  //     console.log(process.env)
  //     const response = await axios.post(`${GET_URL()}/api/search`, {
  //       category: query.get("category") ? query.get("category") : "all",
  //       name: query.get("name") ? query.get("name") : "all",
  //       location: query.get("location") ? query.get("location") : "all"
  //     });
  //     setAdData(response.data.result)

  //     setLoading(false);
  //   } catch (error) {
  //     console.error(error);
  //   }
  // }
  async function getAllAds() {
    try {
      console.log(process.env)
      const response = await axios.get(`${GET_URL()}/api/post/?isApproved=true&isRejected=false`)
      setAdData(response.data.result)

      setLoading(false);
    } catch (error) {
      console.error(error);
    }
  }

  useEffect(() => {
    getAllAds()
  }, [])
  return (
    <>
      <Layout userData={userData} />
      <Tabs />

      <div className="bg-gray-200 min-h-screen">
        <p className="text-4xl font-bold mb-4 w-full px-16">Recommended: </p>
        <div className="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 grid-cols-1 gap-1">
          {loading ? <div className="flex justify-center mt-8 w-full items-center text-center mx-auto"><Loader
            type="TailSpin"
            color="#4299e1"
            height={50}
            width={50}
            timeout={90000} //3 secs
          /></div> : adData.map((ad) => {
            // if (ad.isApproved == "true") {
            return <div className="flex justify-center" key={ad._id}>
              <Link to={`/ads/${ad._id}`}>
                <Posts ad={ad} userId={userData} />
              </Link>
            </div>
            // }
          })}
        </div>
      </div>
    </>
  );
}
