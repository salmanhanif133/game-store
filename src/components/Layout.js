import React from "react";

import NavBar from "./NavBar";
import SearchBox from "./SearchBox";

const Layout = (props) => {

  return (
    <div>
      <NavBar userData={props.userData} />
      <SearchBox />
      {props.childern}
    </div>
  )
}
export default Layout;