import React from "react";
import Home from "./Home";
import Post from "./Post";
import Login from "./Login";
import Signin from "./Signin";
import PostAd from "./PostAd";
import MyAds from "./MyAds";
import Profile from "./Profile";
import EditProfile from "./EditProfile";
import AdminPage from "./Admin/Admin";
// import Search from "./Search";
import "../styles/app.css";
import { Switch, Route, Link } from "react-router-dom";
import Posts from "./Admin/Advertisements";
import ShowUsers from "./Admin/ShowUsers";
import ViewProfile from "./ViewProfile";
import Layout from "./Layout";

const axios = require("axios");


const PrivateRoute = ({ component: Component, userData, loading, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      userData === null ? (
        <>
          <Layout userData={userData} />
          <Link to="/login">
            <p className="text-3xl text-center font-bold text-blue-400">Please Login</p>
          </Link>
        </>
      ) : !userData ? (
        <h1 className="hidden">Loading</h1>
      ) : (
            <Component {...props} userData={userData} isAuth={true} />
          )
    }
  />
);

const PrivateHomeRoute = ({ component: Component, userData, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      userData ? (
        <Component {...props} userData={userData} isAuth={true} />
      ) : (
          <Component {...props} userData={userData} isAuth={false} />
        )
    }
  />
);

const Main = ({ userData, loading }) => {
  return (
    <main>
      <Switch>
        {/* <Route exact path="/admin" component={AdminLogin /> */}
        <Route exact path="/admin/dashboard" component={AdminPage} />
        <Route exact path="/admin/advertisements" component={Posts} />
        <Route exact path="/admin/users" component={ShowUsers} />
        <Route
          exact
          path="/profile/:id"
          render={(props) => <ViewProfile {...props} userData={userData} />}
        />
        <Route
          exact
          path="/ads/:id"
          render={(props) => <Post {...props} userData={userData} />}
        />
        <Route exact path={"/login"} component={Login} />
        <Route exact path="/signin" component={Signin} />

        <PrivateRoute
          exact
          path="/postad"
          component={PostAd}
          userData={userData}
        />
        <PrivateRoute
          exact
          path="/myads"
          component={MyAds}
          userData={userData}
        />
        <PrivateRoute
          path={"/profile"}
          component={Profile}
          userData={userData}
        />
        <PrivateRoute
          path={"/editprofile"}
          component={EditProfile}
          userData={userData}
          loading={loading}
        />
        <PrivateHomeRoute path={"/"} component={Home} userData={userData} />
      </Switch>
    </main>
  );
};

export default Main;
